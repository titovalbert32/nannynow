package com.newfarm.nannynow.data.account

import com.newfarm.nannynow.domain.account.AccountEntity
import com.newfarm.nannynow.domain.type.Either
import com.newfarm.nannynow.domain.type.None
import com.newfarm.nannynow.domain.type.Failure

interface AccountRemote {
    fun register(
        email: String,
        name: String,
        password: String,
        token: String,
        userDate: Long
    ): Either<Failure, None>

    fun login(email: String, password: String, token: String): Either<Failure, AccountEntity>

    fun updateToken(userId: Long, token: String, oldToken: String): Either<Failure, None>

    fun editUser(
        userId: Long,
        email: String,
        name: String,
        password: String,
        status: String,
        token: String,
        image: String
    ): Either<Failure, AccountEntity>
}
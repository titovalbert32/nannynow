package com.newfarm.nannynow.data.account

import com.newfarm.nannynow.domain.account.AccountEntity
import com.newfarm.nannynow.domain.type.Either
import com.newfarm.nannynow.domain.type.None
import com.newfarm.nannynow.domain.type.Failure

interface AccountCache {
    fun getToken(): Either<Failure, String>
    fun saveToken(token: String): Either<Failure, None>

    fun logout(): Either<Failure, None>

    fun getCurrentAccount(): Either<Failure, AccountEntity>
    fun saveAccount(account: AccountEntity): Either<Failure, None>

    fun checkAuth(): Either<Failure, Boolean>
}
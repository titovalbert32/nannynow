package com.newfarm.nannynow.domain.account

import com.newfarm.nannynow.domain.type.None
import com.newfarm.nannynow.domain.type.Either
import com.newfarm.nannynow.domain.type.Failure
import com.newfarm.nannynow.domain.interactor.UseCase
import javax.inject.Inject

class Register @Inject constructor(
    private val repository: AccountRepository
) : UseCase<None, Register.Params>() {

    override suspend fun run(params: Params): Either<Failure, None> {
        return repository.register(params.email, params.name, params.password)
    }

    data class Params(val email: String, val name: String, val password: String)
}


package com.newfarm.nannynow.domain.friends

import com.newfarm.nannynow.domain.interactor.UseCase
import com.newfarm.nannynow.domain.type.None
import javax.inject.Inject

class DeleteFriend @Inject constructor(
    private val friendsRepository: FriendsRepository
) : UseCase<None, FriendEntity>() {

    override suspend fun run(params: FriendEntity) = friendsRepository.deleteFriend(params)
}
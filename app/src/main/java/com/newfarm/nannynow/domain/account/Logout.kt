package com.newfarm.nannynow.domain.account

import com.newfarm.nannynow.domain.interactor.UseCase
import com.newfarm.nannynow.domain.type.Either
import com.newfarm.nannynow.domain.type.Failure
import com.newfarm.nannynow.domain.type.None
import javax.inject.Inject

class Logout @Inject constructor(
    private val accountRepository: AccountRepository
) : UseCase<None, None>() {

    override suspend fun run(params: None): Either<Failure, None> = accountRepository.logout()
}
package com.newfarm.nannynow.domain.account

import com.newfarm.nannynow.domain.interactor.UseCase
import com.newfarm.nannynow.domain.type.None
import javax.inject.Inject

class GetAccount @Inject constructor(
    private val accountRepository: AccountRepository
) : UseCase<AccountEntity, None>() {

    override suspend fun run(params: None) = accountRepository.getCurrentAccount()
}
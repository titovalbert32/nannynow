package com.newfarm.nannynow.domain.account

import com.newfarm.nannynow.domain.interactor.UseCase
import com.newfarm.nannynow.domain.type.Either
import com.newfarm.nannynow.domain.type.Failure
import javax.inject.Inject

class EditAccount @Inject constructor(
    private val repository: AccountRepository
) : UseCase<AccountEntity, AccountEntity>() {

    override suspend fun run(params: AccountEntity): Either<Failure, AccountEntity> {
        return repository.editAccount(params)
    }
}
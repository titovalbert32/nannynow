package com.newfarm.nannynow.domain.friends

import com.newfarm.nannynow.domain.interactor.UseCase
import com.newfarm.nannynow.domain.type.None
import javax.inject.Inject

class ApproveFriendRequest @Inject constructor(
    private val friendsRepository: FriendsRepository
) : UseCase<None, FriendEntity>() {

    override suspend fun run(params: FriendEntity) = friendsRepository.approveFriendRequest(params)
}
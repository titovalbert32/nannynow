package com.newfarm.nannynow.domain.media

import android.net.Uri
import com.newfarm.nannynow.domain.interactor.UseCase
import com.newfarm.nannynow.domain.type.None
import javax.inject.Inject

class CreateImageFile @Inject constructor(
    private val mediaRepository: MediaRepository
) : UseCase<Uri, None>() {

    override suspend fun run(params: None) = mediaRepository.createImageFile()
}
package com.newfarm.nannynow.domain.account

import com.newfarm.nannynow.domain.interactor.UseCase
import com.newfarm.nannynow.domain.type.Either
import com.newfarm.nannynow.domain.type.Failure
import com.newfarm.nannynow.domain.type.None
import javax.inject.Inject

class CheckAuth @Inject constructor(
    private val accountRepository: AccountRepository
) : UseCase<Boolean, None>() {

    override suspend fun run(params: None): Either<Failure, Boolean> = accountRepository.checkAuth()
}
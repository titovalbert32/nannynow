package com.newfarm.nannynow.ui.user

import com.newfarm.nannynow.ui.core.BaseActivity
import com.newfarm.nannynow.ui.core.BaseFragment

class UserActivity : BaseActivity() {
    override var fragment: BaseFragment = UserFragment()
}

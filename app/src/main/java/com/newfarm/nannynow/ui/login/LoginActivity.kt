package com.newfarm.nannynow.ui.login

import com.newfarm.nannynow.ui.core.BaseActivity
import com.newfarm.nannynow.ui.core.BaseFragment

class LoginActivity : BaseActivity() {

    override var fragment: BaseFragment = LoginFragment()
}

package com.newfarm.nannynow.ui.friends

import android.view.LayoutInflater
import android.view.ViewGroup
import com.newfarm.nannynow.databinding.ItemFriendRequestBinding
import com.newfarm.nannynow.domain.friends.FriendEntity
import com.newfarm.nannynow.ui.core.BaseAdapter

open class FriendRequestsAdapter : BaseAdapter<FriendRequestsAdapter.FriendRequestViewHolder>() {

    override fun createHolder(parent: ViewGroup): FriendRequestViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemFriendRequestBinding.inflate(layoutInflater)
        return FriendRequestViewHolder(binding)
    }

    class FriendRequestViewHolder(val binding: ItemFriendRequestBinding) : BaseViewHolder(binding.root) {

        init {
            binding.btnApprove.setOnClickListener {
                onClick?.onClick(item, it)
            }
            binding.btnCancel.setOnClickListener {
                onClick?.onClick(item, it)
            }
        }

        override fun onBind(item: Any) {
            (item as? FriendEntity)?.let {
                binding.friend = it
            }
        }
    }
}
package com.newfarm.nannynow.ui.home

import com.newfarm.nannynow.R
import com.newfarm.nannynow.ui.core.BaseFragment


class ChatsFragment : BaseFragment() {
    override val titleToolbar = R.string.chats

    override val layoutId = R.layout.fragment_chats
}

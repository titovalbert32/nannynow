package com.newfarm.nannynow.ui.register

import com.newfarm.nannynow.ui.core.BaseActivity
import com.newfarm.nannynow.ui.core.BaseFragment

class RegisterActivity : BaseActivity() {

    override var fragment: BaseFragment = RegisterFragment()
}

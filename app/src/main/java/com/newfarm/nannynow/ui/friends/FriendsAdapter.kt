package com.newfarm.nannynow.ui.friends

import android.view.LayoutInflater
import android.view.ViewGroup
import com.newfarm.nannynow.databinding.ItemFriendBinding
import com.newfarm.nannynow.domain.friends.FriendEntity
import com.newfarm.nannynow.ui.core.BaseAdapter

open class FriendsAdapter : BaseAdapter<FriendsAdapter.FriendViewHolder>() {

    override fun createHolder(parent: ViewGroup): FriendViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemFriendBinding.inflate(layoutInflater, parent, false)
        return FriendViewHolder(binding)
    }

    class FriendViewHolder(val binding: ItemFriendBinding) : BaseViewHolder(binding.root) {
        init {
            binding.btnRemove.setOnClickListener {
                onClick?.onClick(item, it)
            }
        }

        override fun onBind(item: Any) {
            (item as? FriendEntity)?.let {
                binding.friend = it
            }
        }
    }
}


package com.newfarm.nannynow.ui

import android.app.Application
import dagger.Component
import com.newfarm.nannynow.presentation.injection.AppModule
import com.newfarm.nannynow.presentation.injection.CacheModule
import com.newfarm.nannynow.presentation.injection.RemoteModule
import com.newfarm.nannynow.presentation.injection.ViewModelModule
import com.newfarm.nannynow.ui.account.AccountActivity
import com.newfarm.nannynow.ui.account.AccountFragment
import com.newfarm.nannynow.ui.core.navigation.RouteActivity
import com.newfarm.nannynow.ui.firebase.FirebaseService
import com.newfarm.nannynow.ui.friends.FriendRequestsFragment
import com.newfarm.nannynow.ui.friends.FriendsFragment
import com.newfarm.nannynow.ui.home.ChatsFragment
import com.newfarm.nannynow.ui.home.HomeActivity
import com.newfarm.nannynow.ui.home.LocationFragment
import com.newfarm.nannynow.ui.login.LoginFragment
import com.newfarm.nannynow.ui.register.RegisterActivity
import com.newfarm.nannynow.ui.register.RegisterFragment
import javax.inject.Singleton

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        initAppComponent()
    }

    private fun initAppComponent() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this)).build()
    }
}

@Singleton
@Component(modules = [AppModule::class, CacheModule::class, RemoteModule::class, ViewModelModule::class])
interface AppComponent {

    //activities
    fun inject(activity: RegisterActivity)
    fun inject(activity: RouteActivity)
    fun inject(activity: HomeActivity)
    fun inject(activity: AccountActivity)

    //fragments
    fun inject(fragment: RegisterFragment)
    fun inject(fragment: LoginFragment)
    fun inject(fragment: ChatsFragment)
    fun inject(fragment: FriendsFragment)
    fun inject(fragment: FriendRequestsFragment)
    fun inject(fragment: AccountFragment)
    fun inject(fragment: LocationFragment)

    //services
    fun inject(service: FirebaseService)
}
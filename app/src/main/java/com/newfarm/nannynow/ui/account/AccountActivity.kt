package com.newfarm.nannynow.ui.account

import android.os.Bundle
import com.newfarm.nannynow.ui.App
import com.newfarm.nannynow.ui.core.BaseActivity
import com.newfarm.nannynow.ui.core.BaseFragment

class AccountActivity : BaseActivity() {
    override var fragment: BaseFragment = AccountFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
    }
}

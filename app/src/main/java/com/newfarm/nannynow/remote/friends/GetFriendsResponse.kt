package com.newfarm.nannynow.remote.friends

import com.newfarm.nannynow.domain.friends.FriendEntity
import com.newfarm.nannynow.remote.core.BaseResponse

class GetFriendsResponse (
    success: Int,
    message: String,
    val friends: List<FriendEntity>
) : BaseResponse(success, message)
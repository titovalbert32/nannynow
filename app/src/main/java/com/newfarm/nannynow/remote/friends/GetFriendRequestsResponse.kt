package com.newfarm.nannynow.remote.friends

import com.google.gson.annotations.SerializedName
import com.newfarm.nannynow.domain.friends.FriendEntity
import com.newfarm.nannynow.remote.core.BaseResponse

class GetFriendRequestsResponse (
    success: Int,
    message: String,
    @SerializedName("friend_requests")
    val friendsRequests: List<FriendEntity>
) : BaseResponse(success, message)
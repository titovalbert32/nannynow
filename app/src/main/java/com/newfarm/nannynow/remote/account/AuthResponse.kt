package com.newfarm.nannynow.remote.account

import com.newfarm.nannynow.domain.account.AccountEntity
import com.newfarm.nannynow.remote.core.BaseResponse

class AuthResponse(
    success: Int,
    message: String,
    val user: AccountEntity
) : BaseResponse(success, message)
package com.newfarm.nannynow.remote.core

open class BaseResponse(
    val success: Int,
    val message: String
)
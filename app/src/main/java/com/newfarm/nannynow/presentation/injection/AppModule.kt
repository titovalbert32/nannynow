package com.newfarm.nannynow.presentation.injection

import android.content.Context
import dagger.Module
import dagger.Provides
import com.newfarm.nannynow.data.account.AccountCache
import com.newfarm.nannynow.data.account.AccountRemote
import com.newfarm.nannynow.data.account.AccountRepositoryImpl
import com.newfarm.nannynow.data.friends.FriendsCache
import com.newfarm.nannynow.data.friends.FriendsRemote
import com.newfarm.nannynow.data.friends.FriendsRepositoryImpl
import com.newfarm.nannynow.data.media.MediaRepositoryImpl
import com.newfarm.nannynow.domain.account.AccountRepository
import com.newfarm.nannynow.domain.friends.FriendsRepository
import com.newfarm.nannynow.domain.media.MediaRepository
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideAppContext(): Context = context

    @Provides
    @Singleton
    fun provideAccountRepository(remote: AccountRemote, cache: AccountCache): AccountRepository {
        return AccountRepositoryImpl(remote, cache)
    }

    @Provides
    @Singleton
    fun provideFriendsRepository(remote: FriendsRemote, accountCache: AccountCache, friendsCache: FriendsCache): FriendsRepository {
        return FriendsRepositoryImpl(accountCache, remote, friendsCache)
    }

    @Provides
    @Singleton
    fun provideMediaRepository(context: Context): MediaRepository {
        return MediaRepositoryImpl(context)
    }
}
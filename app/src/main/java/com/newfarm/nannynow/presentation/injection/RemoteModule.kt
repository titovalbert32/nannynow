package com.newfarm.nannynow.presentation.injection

import dagger.Module
import dagger.Provides
import com.newfarm.nannynow.BuildConfig
import com.newfarm.nannynow.data.account.AccountRemote
import com.newfarm.nannynow.data.friends.FriendsRemote
import com.newfarm.nannynow.remote.account.AccountRemoteImpl
import com.newfarm.nannynow.remote.core.Request
import com.newfarm.nannynow.remote.friends.FriendsRemoteImpl
import com.newfarm.nannynow.remote.service.ApiService
import com.newfarm.nannynow.remote.service.ServiceFactory
import javax.inject.Singleton

@Module
class RemoteModule {

    @Singleton
    @Provides
    fun provideApiService(): ApiService = ServiceFactory.makeService(BuildConfig.DEBUG)

    @Singleton
    @Provides
    fun provideAccountRemote(request: Request, apiService: ApiService): AccountRemote {
        return AccountRemoteImpl(request, apiService)
    }

    @Singleton
    @Provides
    fun provideFriendsRemote(request: Request, apiService: ApiService): FriendsRemote {
        return FriendsRemoteImpl(request, apiService)
    }
}
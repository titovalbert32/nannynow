package com.newfarm.nannynow.presentation.injection

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import com.newfarm.nannynow.cache.AccountCacheImpl
import com.newfarm.nannynow.cache.ChatDatabase
import com.newfarm.nannynow.cache.SharedPrefsManager
import com.newfarm.nannynow.data.account.AccountCache
import com.newfarm.nannynow.data.friends.FriendsCache
import javax.inject.Singleton

@Module
class CacheModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideAccountCache(prefsManager: SharedPrefsManager): AccountCache = AccountCacheImpl(prefsManager)

    @Provides
    @Singleton
    fun provideChatDatabase(context: Context): ChatDatabase {
        return ChatDatabase.getInstance(context)
    }

    //TODO посмотреть, как здесь
    @Provides
    @Singleton
    fun provideFriendsCache(chatDatabase: ChatDatabase): FriendsCache {
        return chatDatabase.friendsDao
    }
}
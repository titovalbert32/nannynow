package com.newfarm.chat.ui.home


import android.content.Context
import android.content.pm.PackageManager
import android.location.*
import com.newfarm.chat.R
import com.newfarm.chat.ui.core.BaseFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.newfarm.chat.ui.App
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.CameraPosition
import java.io.IOException

class LocationFragment : BaseFragment(), OnMapReadyCallback {

    companion object {
        const val USER_LOCATION_ZOOM = 17f
    }

    override val titleToolbar = R.string.chats

    override val layoutId = R.layout.fragment_location

    private lateinit var mapView: MapView
    private lateinit var mGoogleMap: GoogleMap

    private var permissionAccessCourseLocation = 0

    private var currentLocation: Location? = null
        set(value) {
            if (value != null) {
                    if (field == null) {
                        val geoCoder = Geocoder(activity)
                        try {
                            val supposedAddress = geoCoder.getFromLocation(value.latitude, value.longitude, 5)?.firstOrNull()
                            supposedAddress?.let {

                                //TODO cache user's address, location are not defined correctly
                                activity?.let {
                                    Toast.makeText(activity, "${activity?.getString(R.string.your_location)}\n" +
                                            " ${supposedAddress.getAddressLine(supposedAddress.maxAddressLineIndex)}?", Toast.LENGTH_LONG).show()
                                }
                            }
                        } catch (e: IOException) {
                            //the network is unavailable
                            Toast.makeText(activity, activity!!.getString(R.string.error_network), Toast.LENGTH_LONG).show()
                        }
                    }
            }
            field = value
            moveCamera(currentLocation)
        }

    private val locationListener = object : LocationListener {
        override fun onLocationChanged(location: Location?) {
            currentLocation = location
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            if (status == LocationProvider.OUT_OF_SERVICE || status == LocationProvider.TEMPORARILY_UNAVAILABLE) {
                Toast.makeText(activity, activity!!.getString(R.string.geolocation_unreachable), Toast.LENGTH_LONG).show()
            }
        }

        override fun onProviderEnabled(provider: String?) {
            Toast.makeText(activity, activity!!.getString(R.string.detecting_geolocation), Toast.LENGTH_LONG).show()
        }

        override fun onProviderDisabled(provider: String?) {
            Toast.makeText(activity, activity!!.getString(R.string.turn_on_geolocation), Toast.LENGTH_LONG).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        App.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    private fun moveCamera(location: Location?) {
        location?.let{
            val latLongLocation = LatLng(currentLocation!!.latitude, currentLocation!!.longitude)

            mGoogleMap.clear()
            mGoogleMap.addMarker(MarkerOptions().position(latLongLocation).title(activity?.getString(R.string.your_location) ?: "").snippet("Marker Description"))
            //For zooming automatically to the location of the marker
            val cameraPosition = CameraPosition.Builder().target(latLongLocation).zoom(USER_LOCATION_ZOOM).build()
            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val rootView = inflater.inflate(R.layout.fragment_location, container, false)
        mapView = rootView.findViewById(R.id.map)
        mapView.onCreate(savedInstanceState)
        mapView.onResume() // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(activity!!.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mapView.getMapAsync(this)
        return rootView
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mGoogleMap = googleMap

        //TODO request permissions to splashActivity
        if ( ContextCompat.checkSelfPermission( activity!!, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions( activity!!, arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION),
                ++permissionAccessCourseLocation )
        }
        if ( ContextCompat.checkSelfPermission( activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions( activity!!, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                ++permissionAccessCourseLocation )
        }

        if ( ContextCompat.checkSelfPermission( activity!!, android.Manifest.permission.ACCESS_COARSE_LOCATION ) == PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission( activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED) {
            val locationManager: LocationManager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0f, locationListener)

            currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
        }
        else {
            // Type your address please
        }
    }
}
